/**
    Does the same like mirror but also places the child like it is given.
**/
module d_mirror(v)
{
    children();
    mirror(v)
    {
        children();
    }
}

/**
    Places the given objects in a cyrcle 'segments' times. Optionalle let you move the objects 'r' away from the center (assumes r is a value and no vector) it will do this 'layers' time adding diff_angle to it rotation and moven 'h' up the z-axis each time.
    Does treat 'center' acordingly.
**/
module revolve(segments = 6, layers = 1, diff_angle = 0, r = 0, h = 10, center = false)
{
    if(center)
    {
        translate([0,0,-(layers-1)*h/2])
        {
            revolve(segments = segments, layers = layers, diff_angle = diff_angle, r = r, h = h, center = false)
            {
                children();
            }
        }
    }
    else
    {
        for( i_layers = [1:layers])
        {   
            translate([0,0,(i_layers-1)*h])
            rotate([0,0,diff_angle * (i_layers-1)])
            {
                for( i_segments = [1:segments])
                {
                    rotate([0,0,(i_segments - 1) * 360 / segments])
                    translate([r,0,0])
                    {
                        children();
                    }
                }
            }
        }
    }
}
