/**
    @Author: Stefan Pasteiner
    @Name: Minkowski based Transformations
    @Description: This file is a collection of Transformations-modules based on the
    minkowski transformation.
    This transformations may behave odd if used on detayled objects or with an secondary object 
    which is nearly as big or bigger than the primary object.
**/
max_render = 1e13;
    
/**
    @param b: if only one child module is given the transformation will take place with a sphere of this size.
    @child 0: object which the transformation schould be based on 
    @child 1..: secondary objects
    @Description: Makes outlying hull of the first object based of a minkowsky transformation with the other childs or a 
    sphere with the size of the given parameter, if there is only one child given. Otherwise the parameter is ignored.
**/
module minkowski_hull_outer(b = 1)
{
    if ( $children == 0 )
    {
        echo("minkowski_hull_outer: Error no child module found. Skipping.");
    }else if ( $children == 2)
    {
        difference()
        {
            minkowski()
            {
                children(0);
                children(1);
            }
            
            children(0);
        }
    }else if ( $children > 2)
    {
        minkowski_hull_outer()
        {
            children(0);
            union()
            {
                for ( i = [ 1 : 1 : $children - 1 ] )
                {
                    children(i);
                }
            }
        }
    }else
    {
        minkowski_hull_outer()
        {
            children(0);
            sphere(b);
        }
    }
        
}

/**
    Just builds the negative of the given childen
**/
module negative()
{
    difference()
    {
        cube(max_render, center = true);
        children();
    }
}



    /**
    @Description: subtracts the second child from the first on each position of its edge
    */
module minkowski_sub()
{
    if ( $children < 2)
    {
        echo("error module minkowski_sub awayts at least 2 children");
    }else
    {
        negative()
        {
            minkowski()
            {
                negative()
                {
                    children(0);
                }
                union()
                {
                    children([1:$children - 1]);
                }
            }
        }
    }
}

