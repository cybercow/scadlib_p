# scadlib_p

A collection of transformations implemented in the (open)scad geometry description language.

Be aware of the fact that minkowski based transformations need a lot of resources to perform.
